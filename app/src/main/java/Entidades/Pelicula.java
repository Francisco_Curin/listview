package Entidades;

/**
 * Created by DUOC on 27-04-2017.
 */

public class Pelicula {
    private String nombre;
    private String estreno;
    private double rating;
    private String UrlImagen;
    private boolean favorito;
    private int rank;

    public Pelicula() {
    }

    public Pelicula(String nombre, String estreno, double rating, String urlImagen, boolean favorito, int rank) {
        this.nombre = nombre;
        this.estreno = estreno;
        this.rating = rating;
        UrlImagen = urlImagen;
        this.favorito = favorito;
        this.rank = rank;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstreno() {
        return estreno;
    }

    public void setEstreno(String estreno) {
        this.estreno = estreno;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getUrlImagen() {
        return UrlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        UrlImagen = urlImagen;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
