package com.example.duoc.viewholders;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import Entidades.Pelicula;
import Entidades.PeliculasAdapter;

public class ListViewMain extends AppCompatActivity {

    private ListView listaIMDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_main);
        listaIMDB=(ListView) findViewById(R.id.ListaIMDB);

        PeliculasAdapter adapter = new PeliculasAdapter(this, getPeliculas());
        listaIMDB.setAdapter(adapter);

    }

    private ArrayList<Pelicula> getPeliculas(){
        Pelicula peliculauno=new Pelicula("Cadena Perpetua",
                "1994",
                9.2,
                "http://www.imdb.com/title/tt0111161/mediaviewer/rm3597327872",
                true,
                2);
        Pelicula peliculados=new Pelicula("Cadena Perpetua",
                "1994",
                9.0,
                "http://www.imdb.com/title/tt0068646/mediaviewer/rm3868604416",
                true,
                2);
        Pelicula peliculatres=new Pelicula("Cadena Perpetua",
                "1994",
                9.0,
                "http://www.imdb.com/title/tt0071562/mediaviewer/rm3060139520",
                true,
                2);
        ArrayList<Pelicula> peliculas=new ArrayList<>();
        peliculas.add(peliculauno);
        peliculas.add(peliculados);
        peliculas.add(peliculatres);


        for (int x = 4; x < 10000; x++){
            Pelicula aux = new Pelicula("El padrino II",
                    "1974",
                    9.0,
                    "https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR1,0,45,67_AL_.jpg",
                    false,
                    4);
            peliculas.add(aux);
        }
        return peliculas;
    }
}
